#==================
#20180706 dada2
#==================

# .gz -> .fastq
#for f in *.gz ; do gunzip -c "$f" > /data/dada2-tutorial/16s_fastq_40s/"${f%.*}" ; done

#==================
# Data processing
#==================

#source("https://bioconductor.org/biocLite.R")
#biocLite("DECIPHER") #這應該要在dockerfile裡裝好 ok
#biocLite("phangorn") #ok
#biocLite("phyloseq") #ok
#TODO: put package installation in Dockerfile
#install.packages('gridExtra')
#install.packages('ggplot2')
#install.packages('vegan')
#install.packages('doSNOW')


library('dada2');packageVersion('dada2')
library('tidyverse')
library('ggplot2')
library('gridExtra')
library('DECIPHER')
library('phangorn')
library('phyloseq')
library('vegan')
library('doSNOW')

`%nin%` = Negate(`%in%`);
args <- commandArgs(trailingOnly = T);
#### Input files
#rawSeqDataFolder <- "/data/16s_fastq_40s";
rawSeqDataFolder <- args[1];
# TODO: r1, r2 patterns may be specified in an excel file in a near future
#Fs.trim <- 289
Fs.trim <- as.numeric(args[2]);
#Rs.trim <- 220
Rs.trim <- as.numeric(args[3]);
#trimLeft <- 13
trimLeft <- as.numeric(args[4]);
#fastaRef <- "/data/taxonomy_reference_database/silva_nr_v132_train_set.fa.gz"
fastaRef <- args[5];
#fastaRef.species <- "/data/taxonomy_reference_database/silva_species_assignment_v132.fa.gz"
fastaRef.species <- args[6]
#sample_mapping <- '/data/sample_mapping.txt';
sample_mapping <- args[7];
#out_path <-  "/data/16s_fastq_40s_out_20180911_test2"
out_path <- args[8];


#rawSeqDataFolder <- "/data/16s_fastq_40s";
# TODO: r1, r2 patterns may be specified in an excel file in a near future
#Fs.trim <- 289
#Rs.trim <- 220
#trimLeft <- 13
#fastaRef <- "/data/taxonomy_reference_database/silva_nr_v132_train_set.fa.gz"
#fastaRef.species <- "/data/taxonomy_reference_database/silva_species_assignment_v132.fa.gz"
#sample_mapping <- '/data/sample_mapping.txt';
#out_path <-  "/data/16s_fastq_40s_out_20180911_test3"





#### Output files
plotQualityProfile_forward <- file.path(out_path, 'plotQualityProfile_forward.png');
plotQualityProfile_reverse <- file.path(out_path, 'plotQualityProfile_reverse.png');
plotErrors_forward <- file.path(out_path, 'plotErrors_forward.png');
plotErrors_reverse <- file.path(out_path, 'plotErrors_reverse.png');
seqtab.nochim.out <- paste0(out_path, "/seqtab.txt");
track.out <- paste0(out_path, "/track_table.txt");
taxa.out <- paste0(out_path, "/OTU_table_taxa_species.txt");
prevdf.out <- paste0(out_path, "/prevdf.txt");
prevdf.plot <- paste0(out_path, "/prevdf.png");
phy_tree_genus <- paste0(out_path, "/phy_tree_genus.pdf");
plot_pcoa_ps2 <- paste0(out_path, "/plot_pcoa_ps2.png");
plot_NMDS <- paste0(out_path, "/plot_NMDS.pdf");
plot_pcoa_uni <- paste0(out_path, "/plot_pcoa_uni.pdf");
adonis_bray.out <- paste0(out_path, "/adonis_bray.txt")
adonis_bray_miss.out <- paste0(out_path, "/adonis_bray_miss.txt")
richness.plot <- paste0(out_path, "/plot_richness.pdf")
tukey_aov_prefix <- paste0(out_path, "/tukey_aov_");
plot_abundance_bar <- paste0(out_path, "/plot_abundance_bar.pdf");
plot_family_abundance_top30 <- paste0(out_path, '/plot_family_abundance_top30.pdf')
taxonomic.out <- paste0(out_path, "/taxonomic.txt")
OTUdf.out <- paste0(out_path, "/OTUdf.txt")
OTU2_tax.out=paste0(out_path, "/OTU2_tax.txt")
OTU_level_freq_prefix=paste0(out_path, "/OUT_level_freq/OTU_level_freq_tax_")
filt_path <- paste0(out_path, "/filtered")


# Getting data
data_path <-"/data/16s_fastq_40s"
list.files(rawSeqDataFolder)

# output folder
dir.create(out_path);
dir.create(paste0(out_path, "/filtered"));
dir.create(paste0(out_path, "/OUT_level_freq"));

# read data
fnFs <- list.files(rawSeqDataFolder, pattern="_R1_001.fastq", full.names = TRUE) %>% sort;
fnRs <- sort(list.files(rawSeqDataFolder, pattern="_R2_001.fastq", full.names = TRUE))

fnFs %>% length %>% paste0('Sample number: ', .) %>% print;

sample.names <- sapply(strsplit(basename(fnFs), "_"), `[`, 1);
#sample.names <- basename(fnFs) %>% strsplit("_") %>% sapply(`[`, 1);
# Examine quality profiles
fnFs[1:2] %>% plotQualityProfile %>% ggsave(plotQualityProfile_forward, plot = .)
fnRs[1:2] %>% plotQualityProfile %>% ggsave(plotQualityProfile_reverse, plot = .)


# Filtering and Trimming
#filt_path <- data_path %>% file.path("filtered") # Place filtered files in filtered/ subdirectory

filtFs <- file.path(filt_path, paste0(sample.names, "_F_filt.fastq.gz"))
filtRs <- file.path(filt_path, paste0(sample.names, "_R_filt.fastq.gz"))
print(fnFs)
print(filtFs)
print(fnRs)
print(filtRs)
print(sample.names)

out <- filterAndTrim(fnFs, filtFs, fnRs, filtRs, trimLeft=trimLeft, truncLen=c(Fs.trim,Rs.trim),
              maxN=0, maxEE=c(2,2), truncQ=2, rm.phix=TRUE,
              compress=TRUE, multithread=TRUE) # On Windows set multithread=FALSE
head(out)

# Dereplication
derepFs <- derepFastq(filtFs, verbose=TRUE)
derepRs <- derepFastq(filtRs, verbose=TRUE)
# Name the derep-class objects by the sample names
names(derepFs) <- sample.names
names(derepRs) <- sample.names

# Learn the Error Rates (久)
errF <- learnErrors(filtFs, multithread=TRUE)
errR <- learnErrors(filtRs, multithread=TRUE)

plotErrors(errF, nominalQ=TRUE) %>% ggsave(plotErrors_forward, plot=.);
plotErrors(errR, nominalQ=TRUE) %>% ggsave(plotErrors_reverse, plot=.);

# Sample Inference (久)
dadaFs <- dada(derepFs, err=errF, multithread=TRUE)
dadaRs <- dada(derepRs, err=errR, multithread=TRUE)


# Merge paired reads
mergers <- mergePairs(dadaFs, derepFs, dadaRs, derepRs, verbose=TRUE)

# Construct sequence table
seqtab <- makeSequenceTable(mergers)
#dim(seqtab)
#table(nchar(getSequences(seqtab)))

# Remove chimeras
seqtab.nochim <- removeBimeraDenovo(seqtab, method="consensus", multithread=TRUE, verbose=TRUE)
#dim(seqtab.nochim)
(sum(seqtab.nochim)/sum(seqtab)) %>% paste0('Chimeras ratio: ', .) %>% print;
write.table(seqtab.nochim,file=seqtab.nochim.out,quote=F,sep="\t",row.names=T)

# Track reads through the pipeline
getN <- function(x) sum(getUniques(x))
track <- cbind(out, sapply(dadaFs, getN), sapply(dadaRs, getN), sapply(mergers, getN), rowSums(seqtab.nochim))
# If processing a single sample, remove the sapply calls: e.g. replace sapply(dadaFs, getN) with getN(dadaFs)
colnames(track) <- c("input", "filtered", "denoisedF", "denoisedR", "merged", "nonchim")
rownames(track) <- sample.names
write.table(track,file=track.out,quote=F,sep="\t",row.names=T)

# Assign taxonomy
# reference database
taxa <- assignTaxonomy(seqtab.nochim, refFasta = fastaRef, multithread=TRUE)
taxa <- addSpecies(taxa, refFasta = fastaRef.species)
write.table(taxa,file=taxa.out, quote=F,sep="\t",row.names=T)



# Construct phylogenetic tree
seqs <- getSequences(seqtab.nochim)
names(seqs) <- seqs # This propagates to the tip labels of the tree
alignment <- AlignSeqs(DNAStringSet(seqs), anchor=NA,verbose=TRUE)

phangAlign <- phyDat(as(alignment, "matrix"), type="DNA")
dm <- dist.ml(phangAlign)
treeNJ <- NJ(dm) # Note, tip order != sequence order
fit = pml(treeNJ, data=phangAlign)
fitGTR <- update(fit, k=4, inv=0.2)
fitGTR <- optim.pml(fitGTR, model="GTR", optInv=TRUE, optGamma=TRUE,
        rearrangement = "stochastic", control = pml.control(trace = 0))
detach("package:phangorn", unload=TRUE)


#==================
# Data analysis
#==================
# sample data

sample_group <- read.delim(sample_mapping,header=T)
rownames(sample_group) = sample_group[,1]

# construct a phyloseq object
ps <- phyloseq(otu_table(seqtab.nochim, taxa_are_rows=FALSE), 
               sample_data(sample_group), 
               tax_table(taxa),phy_tree(fitGTR$tree))
ps <- prune_samples(sample_names(ps) != "Mock", ps) # Remove mock sample
#ps
#phyloseq-class experiment-level object
#otu_table()   OTU Table:         [ 2549 taxa and 39 samples ]
#sample_data() Sample Data:       [ 39 samples by 16 sample variables ]
#tax_table()   Taxonomy Table:    [ 2549 taxa by 7 taxonomic ranks ]
#phy_tree()    Phylogenetic Tree: [ 2549 tips and 2547 internal nodes ]


# Taxonomic Filtering
# table(tax_table(ps)[, "Phylum"], exclude = NULL) # Create table, number of features for each phyla
ps <- subset_taxa(ps, !is.na(Phylum) & !Phylum %in% c("", "uncharacterized"))
# 7 ASV deleted
#phyloseq-class experiment-level object
#otu_table()   OTU Table:         [ 2541 taxa and 39 samples ]
#sample_data() Sample Data:       [ 39 samples by 16 sample variables ]
#tax_table()   Taxonomy Table:    [ 2541 taxa by 7 taxonomic ranks ]
#phy_tree()    Phylogenetic Tree: [ 2541 tips and 2539 internal nodes ]

# Compute prevalence of each feature, store as data.frame
prevdf = apply(X = otu_table(ps),
               MARGIN = ifelse(taxa_are_rows(ps), yes = 1, no = 2),
               FUN = function(x){sum(x > 0)})
# Add taxonomy and total read counts to this data.frame
prevdf = data.frame(Prevalence = prevdf,
                    TotalAbundance = taxa_sums(ps),
                    tax_table(ps))
write.table(prevdf,file=prevdf.out,quote=F,sep="\t",row.names=F)


# Prevalence Filtering
# Subset to the remaining phyla
(ggplot(prevdf, aes(TotalAbundance, Prevalence / nsamples(ps),color=Phylum)) +
  # Include a guess for parameter
  geom_hline(yintercept = 0.05, alpha = 0.5, linetype = 2) +  geom_point(size = 2, alpha = 0.7) +
  scale_x_log10() +  xlab("Total Abundance") + ylab("Prevalence [Frac. Samples]") +
  facet_wrap(~Phylum) + theme(legend.position="none")) %>% ggsave(prevdf.plot, plot = .);

# Define prevalence threshold as 5% of total samples
prevalenceThreshold <- 0.05 * nsamples(ps)
keepTaxa <- rownames(prevdf)[(prevdf$Prevalence >= prevalenceThreshold)]
ps2 <-prune_taxa(keepTaxa, ps)
#ps2
#phyloseq-class experiment-level object
#otu_table()   OTU Table:         [ 1084 taxa and 39 samples ]
#sample_data() Sample Data:       [ 39 samples by 16 sample variables ]
#tax_table()   Taxonomy Table:    [ 1084 taxa by 7 taxonomic ranks ]
#phy_tree()    Phylogenetic Tree: [ 1084 tips and 1083 internal nodes ]

# taxa tree
sampleNames = colnames(sample_group);
sampleNames = sampleNames[8:length(sampleNames)] %>% make.names;


ex1 <- names(sort(taxa_sums(ps2), decreasing = TRUE)[1:20]) %>% prune_taxa(., ps2);


(lapply(1:length(sampleNames), function(i) {
  plot_tree(ex1, color = sampleNames[i], label.tips = "Genus", size = "Abundance") %>% return;
})) %>% marrangeGrob(ncol=2, nrow=2) %>% ggsave(phy_tree_genus, .,width=16, height=12);



# beta-diversity
# remove samples which <10000 reads，有 2542 ASVs
#ps_10000 <- prune_samples(rowSums(otu_table(ps)) > 10000, ps)

#ps2 prevalence >2，有 1085 ASVs

# Perform a PCoA using Bray-Curtis dissimilarity 例子
pslog_ps2 <- transform_sample_counts(ps2, function(x) log(1 + x))
out.pcoa.log.ps2 <- ordinate(pslog_ps2,  method = "MDS", distance = "bray")
evals_ps2 <- out.pcoa.log.ps2$values[,1]


# Transform data to proportions as appropriate for Bray-Curtis distances
ps.prop <- transform_sample_counts(ps2, function(otu) otu/sum(otu))
ord.nmds.bray <- ordinate(ps.prop, method="NMDS", distance="bray")

(lapply(1:length(sampleNames), function(i) {
  (plot_ordination(ps.prop, ord.nmds.bray, color=sampleNames[i], title=paste0("Bray NMDS: ", sampleNames[i])) + theme(legend.title=element_blank())
) %>% return;
})) %>% marrangeGrob(ncol=2, nrow=2) %>% ggsave(plot_NMDS, .,width=16, height=12);


# principal coordinates analysis (PCoA) on the weighted Unifrac distance
ps2.log <- transform_sample_counts(ps2, function(x) log(1 + x))
out.wuf.log <- ordinate(ps2.log, method = "MDS", distance = "wunifrac")
evals <- out.wuf.log$values$Eigenvalues;

(lapply(1:length(sampleNames), function(i) {
  (plot_ordination(ps2.log, out.wuf.log, color = sampleNames[i], title=paste0("PCoA plot (the weighted Unifrac distance): ", sampleNames[i])) + 
    theme(legend.title=element_blank())) %>%
    # + coord_fixed(sqrt(evals[2] / evals[1])))
    return;
})) %>% marrangeGrob(ncol=2, nrow=2) %>% ggsave(plot_pcoa_uni, plot=.,width=16, height=12);



# vegan package (PERMANOVA) - use adonis

adonis_bray <- sampleNames %>% 
                paste(collapse='+') %>%
                paste0('distance(ps.prop, method="bray")', ' ~ ', .) %>%
                as.formula %>%
                adonis(permutations = 999, ., data = sample_group);

#adonis_bray <- adonis(permutations = 999, distance(ps.prop, method="bray") ~ mut_no_group+hc_cluster_ori+hc_cluster_scaled_ori+hc_cluster_pro+hc_cluster_scaled_pro+k_means_raw_ori+k_mean_scaled_ori+k_means_raw_pro+k_mean_scaled_pro, data = sample_group)
format(data.frame(adonis_bray$aov.tab), digits=2) %>% 
  write.table(file=adonis_bray.out,quote=F,sep="\t",row.names=T)

# some samples may be dropped by the above model. Recover these missing samples.
#adonis_bray_miss <- which(sampleNames %nin% rownames(adonis_bray$aov.tab)) %>% sampleNames[.] %>% 
#                      paste(collapse='+') %>%
#                      paste0('distance(ps.prop, method="bray")', ' ~ ', .) %>%
#                      as.formula %>% 
#                      adonis(permutations = 999, ., data = sample_group)
#format(data.frame(adonis_bray_miss$aov.tab), digits=2) %>%
#  write.table(file=adonis_bray_miss.out,quote=F,sep="\t",row.names=T);
if (length(which(sampleNames %nin% rownames(adonis_bray$aov.tab)))>0){
  adonis_bray_miss <- which(sampleNames %nin% rownames(adonis_bray$aov.tab)) %>% sampleNames[.] %>% 
                      paste(collapse='+') %>%
                      paste0('distance(ps.prop, method="bray")', ' ~ ', .) %>%
                      as.formula %>% 
                      adonis(permutations = 999, ., data = sample_group)
  format(data.frame(adonis_bray_miss$aov.tab), digits=2) %>%
  write.table(file=adonis_bray_miss.out,quote=F,sep="\t",row.names=T)
}else print ("no miss");


# alpha-diversity
measures = c("Observed","Shannon", "Chao1","ACE");
(lapply(1:length(sampleNames), function(i) {
  plot_richness(ps2, x=sampleNames[i], measures=measures, color=sampleNames[i])
})) %>% marrangeGrob(ncol=1, nrow=2) %>% ggsave(richness.plot, plot=.,width=16, height=12);


# Alpha statistics
er_ps2 <- estimate_richness(ps2, split = TRUE, measures=measures)
richness_ps2 <- data.frame(er_ps2, sample_data(ps2))

lapply(measures, function(measure){
  tukey.aov <- sampleNames %>% 
    paste(collapse='+') %>% 
    paste0(measure , ' ~ ', .) %>% 
    as.formula %>%
    aov(data=richness_ps2) %>%
    TukeyHSD;
  tukey.aov.p <- foreach(i = 1:length(names(tukey.aov)),.combine=rbind, .packages=c('tidyverse')) %dopar% {
    cbind(names(tukey.aov)[i], tukey.aov[[names(tukey.aov)[i]]])
  }
  colnames(tukey.aov.p)[1] = 'col';

#  tukey.aov.miss <- which(sampleNames %nin% names(tukey.aov)) %>%
#        sampleNames[.] %>%
#        paste(collapse='+') %>%
#        paste0(measure , ' ~ ', .) %>%
#        as.formula %>%
#        aov(data=richness_ps2) %>%
#        TukeyHSD;
#  tukey.aov.p.miss <- foreach(i = 1:length(names(tukey.aov.miss)),.combine=rbind, .packages=c('tidyverse')) %dopar% {
#    cbind(names(tukey.aov.miss)[i], tukey.aov.miss[[names(tukey.aov.miss)[i]]])
#  }
#  rbind(tukey.aov.p, tukey.aov.p.miss) %>%
#    write.table(file=paste0(tukey_aov_prefix, measure, '.txt'),quote=F,sep="\t",row.names=T)
  if (length(which(sampleNames %nin% names(tukey.aov)))>0) {
    tukey.aov.miss <- which(sampleNames %nin% names(tukey.aov)) %>%
        sampleNames[.] %>%
        paste(collapse='+') %>%
        paste0(measure , ' ~ ', .) %>%
        as.formula %>%
        aov(data=richness_ps2) %>%
        TukeyHSD;
    tukey.aov.p.miss <- foreach(i = 1:length(names(tukey.aov.miss)),.combine=rbind, .packages=c('tidyverse')) %dopar% {
    cbind(names(tukey.aov.miss)[i], tukey.aov.miss[[names(tukey.aov.miss)[i]]])
  }
  rbind(tukey.aov.p, tukey.aov.p.miss) %>%
    write.table(file=paste0(tukey_aov_prefix, measure, '.txt'),quote=F,sep="\t",row.names=T)
  }else write.table(tukey.aov.p, file=paste0(tukey_aov_prefix, measure, '.txt'),quote=F,sep="\t",row.names=T) 
});

#=====================================
# Abundance
ps.prop <- transform_sample_counts(ps2, function(otu) otu/sum(otu))

# Phylum

(lapply(1:length(sampleNames), function(i) {
  plot_bar(ps.prop, x=sampleNames[i], title=paste0("Abundance: ", sampleNames[i]), fill="Phylum");
})) %>% marrangeGrob(ncol=2, nrow=2) %>% ggsave(plot_abundance_bar, .,width=16, height=12);

############################################################################################################################################# Break point 20181017

# Family
myTaxa.top30 = names(sort(taxa_sums(ps2), decreasing = TRUE)[1:30])
ps.top30 <- prune_taxa(myTaxa.top30, ps.prop)

(lapply(sampleNames, function(sampleName) {
  plot_bar_top30_before <- plot_bar(ps.top30, x=sampleName, fill="Family");
  Merged <- merge_samples(ps.top30, sampleName)
  sample_data(Merged)[[sampleName]] <- factor(sample_names(Merged));
  Merged <- transform_sample_counts(Merged,function(x) 100 * x/sum(x))
  plot_bar_top30_after <- plot_bar(Merged, sampleName, "Abundance", "Family")
  return(list(plot_bar_top30_before, plot_bar_top30_after))
})) %>% unlist(recursive = FALSE) %>% marrangeGrob(ncol=2, nrow=2, layout_matrix=matrix(c(1,3,2,4), 2)) %>% ggsave(plot_family_abundance_top30, .,width=16, height=12);

# ==================================================
# Agglomerate taxa
# How many genera would be present after filtering?

taxonomic.ranks = c("Phylum", "Class", "Order", "Family", "Genus", "Species");

sapply(taxonomic.ranks, function(taxonomic.rank){
  length(get_taxa_unique(ps2, taxonomic.rank = taxonomic.rank))
}) %>% t %>%
  write.table(file=taxonomic.out,quote=F,sep="\t",row.names=F)

# output ps2
OTU1 <- ps2 %>% otu_table %>% as("matrix")
if (taxa_are_rows(ps2)) { OTU1 <- t(OTU1) }
OTU1 %>% as.data.frame %>% write.table(OTUdf.out, quote=F, sep="\t", row.names=T);

ps %>% tax_table %>% as("matrix") %>% as.data.frame %>% write.table(OTU2_tax.out, quote=F, sep="\t", row.names=T);

# Show abundance at different level (Phylum, Family, Genus)
# for LEfSe
lapply(taxonomic.ranks, function(taxonomic.rank){
  # OTU_levels_prefix=paste0(out_path, "/OTU_levels_tax_")
  # OTU_level_freq_prefix=paste0(out_path, "/OTU_level_freq_")
  ps3 = tax_glom(ps2, taxonomic.rank, NArm = TRUE)
  ps3ra = transform_sample_counts(ps3, function(x){x / sum(x)})
  OTU3freq = as(otu_table(ps3ra),"matrix")
  if(taxa_are_rows(ps3ra)){OTU3freq <- t(OTU3freq)}
  OTU3freq %>% as.data.frame %>% t %>% 
    cbind((ps3 %>% tax_table %>% as("matrix") %>% as.data.frame), .) %>%
    write.table(.,file=paste0(OTU_level_freq_prefix, taxonomic.rank, '.txt'),quote=F,sep="\t",row.names=T)
}) -> temp;

