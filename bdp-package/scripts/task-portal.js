require("bdp-task-adapter")
  .readAndExecute(require("path").join(__dirname, "task-index.yaml"))
  .then(() => process.exit(0))
  .catch(e => {
    console.log(e);
    process.exit(1);
  });
