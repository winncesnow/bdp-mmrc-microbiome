# Base Image
FROM biocontainers/biocontainers:latest

# Install packages that needs root privileges
USER root
ENV NODE_PATH=/usr/lib/node_modules
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get update && \
    apt-get install -y nodejs && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E084DAB9 && \
    echo "deb https://cloud.r-project.org/bin/linux/ubuntu xenial-cran35/" | tee -a /etc/apt/sources.list && \
    apt-get update && \
    apt-get install -y r-base && \
    apt-get install -y libxml2-dev libcurl4-openssl-dev libssl-dev libnetcdf-dev && \
    apt-get clean && \
    apt-get purge --auto-remove -y curl
#Install required packages
USER biodocker
ENV PATH R_LIBS=~/Rlibs:$PATH
ENV R_LIBS_USER=~/Rlibs
RUN mkdir ~/Rlibs && \
    R --vanilla -e 'install.packages(c("tidyverse", "gridExtra", "vegan", "doSNOW","svglite"), repos="http://cran.us.r-project.org", dependencies =T)' && \
    R --vanilla -e "source('https://bioconductor.org/biocLite.R');biocLite(c('dada2','DECIPHER','phangorn','phyloseq'), ask=F);"

COPY --chown=biodocker:biodocker ["./docker-wrapper.js", "./bdp-tasks.yaml", "/home/biodocker/"]
COPY --chown=biodocker:biodocker ["./bdp-package", "/home/biodocker/bdp-package"]

RUN npm install bdp-docker-wrapper --prefix /home/biodocker && \
    npm install bdp-task-adapter@next --prefix /home/biodocker/bdp-package/scripts

# Copy your scripts
COPY --chown=biodocker:biodocker ["./swli-scripts", "/home/biodocker/scripts/"]

USER root
ENTRYPOINT ["node", "/home/biodocker/docker-wrapper.js"]
